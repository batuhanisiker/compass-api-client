package compass.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RateUpdate {

    private Integer minStay;
    private String paxOption;
    private Integer paxNumber;
    private Float rate;
    private Integer minAge;
    private Integer maxAge;

    public Integer getMinStay() {
        return minStay;
    }

    public String getPaxOption() {
        return paxOption;
    }

    public Integer getPaxNumber() {
        return paxNumber;
    }

    public Float getRate() {
        return rate;
    }

    public void setMinStay(Integer minStay) {
        this.minStay = minStay;
    }

    public void setPaxOption(String paxOption) {
        this.paxOption = paxOption;
    }

    public void setPaxNumber(Integer paxNumber) {
        this.paxNumber = paxNumber;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public void setMinAge(Integer minAge) {
        this.minAge = minAge;
    }

    public void setMaxAge(Integer maxAge) {
        this.maxAge = maxAge;
    }

    public Integer getMinAge() {
        return minAge;
    }

    public Integer getMaxAge() {
        return maxAge;
    }
}
