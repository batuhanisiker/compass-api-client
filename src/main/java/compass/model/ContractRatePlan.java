package compass.model;


public class ContractRatePlan {

    private Integer id;

    private String name;

    private String status;

    private boolean isManageableByCm;

    private ContractCurrency contractCurrency;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public boolean getIsManageableByCm() {
        return isManageableByCm;
    }

    public ContractCurrency getContractCurrency() {
        return contractCurrency;
    }

    public void setContractCurrency(ContractCurrency contractCurrency) {
        this.contractCurrency = contractCurrency;
    }
}
