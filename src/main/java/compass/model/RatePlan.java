package compass.model;

public class RatePlan {

    private AllotmentPlan allotmentPlan;
    private Integer id;
    private ContractCurrency contractCurrency;
    private String name;
    private RatePlan parent;

    public AllotmentPlan getAllotmentPlan() {
        return allotmentPlan;
    }

    public void setAllotmentPlan(AllotmentPlan allotMentPlan) {
        this.allotmentPlan = allotMentPlan;

    }

    public Integer getId() {
        return id;
    }

    public ContractCurrency getContractCurrency() {
        return contractCurrency;
    }

    public String getName() {
        return name;
    }

    public RatePlan getParent() {
        return parent;
    }
}