package compass.model;

public class ContractRoom {

    private Integer id;

    private String name;

    private String status;

    private Integer adultPax;

    private String adultMinPax;

    private String adultMaxPax;

    private Integer childMaxPax;

    private Integer babyMaxAge;

    private Integer childMaxAge;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public Integer getAdultPax() {
        return adultPax;
    }

    public String getAdultMinPax() {
        return adultMinPax;
    }

    public String getAdultMaxPax() {
        return adultMaxPax;
    }

    public Integer getChildMaxPax() {
        return childMaxPax;
    }

    public Integer getBabyMaxAge() {
        return babyMaxAge;
    }

    public Integer getChildMaxAge() {
        return childMaxAge;
    }
}
