package compass.model.Booking;

public class BookingUpdatedAt {

    private String date;

    public String getDate() {
        return date;
    }

    public void setUpdatedAt(String date) {
        this.date = date;
    }
}
