package compass.model;

public class RatePlanRoomRateContractRoom {

    private Integer id;
    private String name;
    private Integer babyMaxAge;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getBabyMaxAge() {
        return babyMaxAge;
    }
}
