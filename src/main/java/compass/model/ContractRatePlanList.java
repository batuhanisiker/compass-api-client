package compass.model;

import java.util.List;

public class ContractRatePlanList {
    private List<ContractRatePlan> ratePlans;

    public List<ContractRatePlan> getRatePlans() {
        return ratePlans;
    }
}
