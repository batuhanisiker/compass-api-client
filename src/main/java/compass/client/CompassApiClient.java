package compass.client;

import compass.client.service.DataService;
import compass.client.service.compass.*;
import compass.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompassApiClient {

    private String consumerKey;
    private String hotelCode;

    public CompassApiClient(String consumerKey, String hotelCode) {
        this.consumerKey = consumerKey;
        this.hotelCode = hotelCode;
    }

    @Autowired
    private ContractRoomService contractRoomService;

    @Autowired
    private ContractRatePlanService contractRatePlanService;

    @Autowired
    private AllotmentPlanRestrictionService allotmentPlanRestrictionService;

    @Autowired
    private AllotmentPlanRoomService allotmentPlanRoomService;

    @Autowired
    private RatePlanRoomRateService ratePlanRoomRateService;

    @Autowired
    private DataService dataService;

    public ResponseEntity<RatePlan> getRatePlan(
            Integer ratePlanCode,
            String embedString){

        return dataService.getRatePlan(hotelCode, ratePlanCode, embedString, consumerKey);
    }


    public ResponseEntity<ContractRoom> getContractRoom(String contractId, Integer roomCode){
        return dataService.getContractRoom(contractId, roomCode, consumerKey);
    }

    public ResponseEntity<List<ContractRoom>> getContractRoomList(String contractId){
        return contractRoomService.getContractRoomList(consumerKey, contractId);
    }

    public ResponseEntity<ContractRatePlanList> getContractRatePlan(String contractId){
        return contractRatePlanService.getContractRatePlan(consumerKey, contractId);
    }

    public ResponseEntity<ContractRatePlanList> getContractRatePlanWithEmbed(String contractId, String embed){
        return contractRatePlanService.getContractRatePlan(consumerKey, contractId, embed);
    }

    public ResponseEntity<List<AllotmentPlanRoom>> getAllotmentPlanRoom(
            Integer allotmentPlanId,
            String fromDate,
            String toDate,
            Integer roomCode
    ){
        return allotmentPlanRoomService.getAllotmentPlanRoomList(
                consumerKey,
                hotelCode,
                allotmentPlanId,
                fromDate,
                toDate,
                roomCode
        );
    }

    public void createAlotmentPlanRoom(String allotmentPlanId,
                                     AllotmentPlanRoomUpdate allotmentPlanRoomUpdate){
        allotmentPlanRoomService.createAllotmentPlanRoom(
                consumerKey,
                hotelCode,
                allotmentPlanId,
                allotmentPlanRoomUpdate
        );
    }

    public ResponseEntity<List<AllotmentPlanRestriction>> getAllotmentPlanRestrictionList(
            Integer allotmentPlanId,
            String fromDate,
            String endDate,
            Integer contractRoomId
    ){
        return allotmentPlanRestrictionService.getAllotmentPlanRestrictionList(
                consumerKey,
                hotelCode,
                allotmentPlanId,
                fromDate,
                endDate,
                contractRoomId
        );
    }

    public void createAllotmentPlanRestriction(Integer allotmenntPlanId,
                                               String fromDate,
                                               String toDate,
                                               Integer roomCode,
                                               AllotmentPlanRestrictionUpdate allotmentPlanRestrictionUpdate
    ){
        allotmentPlanRestrictionService.createAllotmentPlanRestriction(
                consumerKey,
                hotelCode,
                allotmenntPlanId,
                fromDate,
                toDate,
                roomCode,
                allotmentPlanRestrictionUpdate
        );
    }

    public void removeAllotmentPlanRestriction(Integer allotmenntPlanId,
                                               String fromDate,
                                               String toDate,
                                               Integer roomCode,
                                               AllotmentPlanRestrictionUpdate allotmentPlanRestrictionUpdate
    ) {
        allotmentPlanRestrictionService.removeAllotmentPlanRestriction(
                consumerKey,
                hotelCode,
                allotmenntPlanId,
                fromDate,
                toDate,
                roomCode,
                allotmentPlanRestrictionUpdate
        );
    }

    public ResponseEntity<List<RatePlanRoomRate>> getRatePlanRoomRateResponseEntity(
            Integer ratePlanId,
            String fromDate,
            String toDate,
            String embed
    ){
        return   ratePlanRoomRateService.getRatePlanRoomRateList(
              consumerKey, hotelCode, ratePlanId, fromDate, toDate, embed);
    }
}
