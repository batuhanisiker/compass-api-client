package compass.client.service;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("compass_api_host")
public class ServiceProperties {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
