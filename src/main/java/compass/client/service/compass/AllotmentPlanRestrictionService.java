package compass.client.service.compass;

import compass.client.service.HeaderService;
import compass.client.service.HelperEntityService;
import compass.client.service.ServiceProperties;
import compass.model.AllotmentPlanRestriction;
import compass.model.AllotmentPlanRestrictionUpdate;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@EnableConfigurationProperties(ServiceProperties.class)
public class AllotmentPlanRestrictionService {

    private final ServiceProperties serviceProperties;

    private HelperEntityService helperEntityService;

    private RestTemplate restTemplate;

    public AllotmentPlanRestrictionService(ServiceProperties serviceProperties,
                                           HelperEntityService helperEntityService,
                                           RestTemplate restTemplate) {
        this.serviceProperties = serviceProperties;
        this.helperEntityService = helperEntityService;
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<List<AllotmentPlanRestriction>> getAllotmentPlanRestrictionList(
            String consumerKey,
            String contractId,
            Integer allotmentPlanId,
            String fromDate,
            String endDate,
            Integer roomTypeCode
    ){
        HttpEntity<String> entity = helperEntityService.httpEntity(consumerKey, contractId);
        ResponseEntity<List<AllotmentPlanRestriction>> allotmentPlanRestrictionResponseEntity = restTemplate.exchange(
                serviceProperties.getUrl() + "/allotment-plans/" + allotmentPlanId +
                        "/restrictions?fromDate=" + fromDate + "&toDate=" +endDate + "&contractRoomId=" + roomTypeCode,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<AllotmentPlanRestriction>>() {
                });

        return allotmentPlanRestrictionResponseEntity;

    }

    public void createAllotmentPlanRestriction(
            String consumerKey,
            String contractId,
            Integer allotmentPlanId,
            String fromDate,
            String toDate,
            Integer roomCode,
            AllotmentPlanRestrictionUpdate allotmentPlanRestrictionUpdate){

        HeaderService headerService = new HeaderService();
        HttpHeaders headers = headerService.httpHeaderService(consumerKey, contractId);

        HttpEntity<AllotmentPlanRestrictionUpdate> allotmentPlanRoomUpdateHttpEntity = new HttpEntity<>(
                allotmentPlanRestrictionUpdate,
                headers
        );

        restTemplate.exchange(
                serviceProperties.getUrl() +  "/allotment-plans/" + allotmentPlanId +
                        "/restrictions?fromDate=" + fromDate + "&toDate=" +
                        toDate + "&contractRoomId=" + roomCode,
                HttpMethod.POST,
                allotmentPlanRoomUpdateHttpEntity,
                AllotmentPlanRestrictionUpdate.class
        );
    }

    public void removeAllotmentPlanRestriction(
            String consumerKey,
            String contractId,
            Integer allotmentPlanId,
            String fromDate,
            String toDate,
            Integer roomCode,
            AllotmentPlanRestrictionUpdate allotmentPlanRestrictionUpdate
    ) {

        HeaderService headerService = new HeaderService();
        HttpHeaders headers = headerService.httpHeaderService(consumerKey, contractId);

        HttpEntity<AllotmentPlanRestrictionUpdate> allotmentPlanRoomUpdateHttpEntity = new HttpEntity<>(
                allotmentPlanRestrictionUpdate,
                headers
        );

        restTemplate.exchange(
                serviceProperties.getUrl() +  "/allotment-plans/" + allotmentPlanId +
                        "/restrictions?fromDate=" + fromDate + "&toDate=" +
                        toDate + "&contractRoomId=" + roomCode,
                HttpMethod.DELETE,
                allotmentPlanRoomUpdateHttpEntity,
                AllotmentPlanRestrictionUpdate.class
        );
    }
}
