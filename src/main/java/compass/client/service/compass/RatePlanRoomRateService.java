package compass.client.service.compass;

import compass.client.service.HelperEntityService;
import compass.client.service.ServiceProperties;
import compass.model.RatePlanRoomRate;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@EnableConfigurationProperties(ServiceProperties.class)
public class RatePlanRoomRateService {

    private final ServiceProperties serviceProperties;

    private RestTemplate restTemplate;

    private HelperEntityService helperEntityService;

    public RatePlanRoomRateService(ServiceProperties serviceProperties,
                                   RestTemplate restTemplate,
                                   HelperEntityService helperEntityService) {
        this.serviceProperties = serviceProperties;
        this.helperEntityService = helperEntityService;
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<List<RatePlanRoomRate>> getRatePlanRoomRateList(
            String consumerKey,
            String contractId,
            Integer ratePlanId,
            String fromDate,
            String toDate,
            String embed
    ){
        HttpEntity<String> entity = helperEntityService.httpEntity(consumerKey, contractId);
        ResponseEntity<List<RatePlanRoomRate>> ratePlanRoomRateResponseEntity =
                restTemplate.exchange(
                        serviceProperties.getUrl() + "/rate-plans/" +
                                ratePlanId + "/rooms/rates?"
                                + "fromDate=" + fromDate + "&toDate=" + toDate +
                                "&embed="+  embed,
                        HttpMethod.GET, entity, new ParameterizedTypeReference<List<RatePlanRoomRate>>() {
                        });

        return ratePlanRoomRateResponseEntity;
    }
}
