package compass.client.service.compass;

import compass.client.service.HeaderService;
import compass.client.service.HelperEntityService;
import compass.client.service.ServiceProperties;
import compass.model.AllotmentPlanRoom;
import compass.model.AllotmentPlanRoomUpdate;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@EnableConfigurationProperties
public class AllotmentPlanRoomService {

    private final ServiceProperties serviceProperties;

    private RestTemplate restTemplate;

    private HelperEntityService helperEntityService;

    public AllotmentPlanRoomService(ServiceProperties serviceProperties,
                                    RestTemplate restTemplate,
                                    HelperEntityService helperEntityService) {
        this.serviceProperties = serviceProperties;
        this.restTemplate = restTemplate;
        this.helperEntityService = helperEntityService;
    }

    public ResponseEntity<List<AllotmentPlanRoom>> getAllotmentPlanRoomList(
            String consumerKey,
            String hotelCode,
            Integer allotmentPlanId,
            String fromDate,
            String toDate,
            Integer roomCode
            ){

        HttpEntity<String> entity = helperEntityService.httpEntity(consumerKey, hotelCode);
        ResponseEntity<List<AllotmentPlanRoom>> allotmentPlanResponseEntity = restTemplate.exchange(
                serviceProperties.getUrl() + "/allotment-plans/" + allotmentPlanId +
                        "/rooms?fromDate=" + fromDate + "&toDate=" +toDate + "&contractRoomId=" + roomCode,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<AllotmentPlanRoom>>() {
                });

        return allotmentPlanResponseEntity;
    }

    public void createAllotmentPlanRoom(
            String consumerKey,
            String contractId,
            String allotmentPlanId,
            AllotmentPlanRoomUpdate allotmentPlanRoomUpdate
    ){
        HeaderService headerService = new HeaderService();
        HttpHeaders headers = headerService.httpHeaderService(consumerKey, contractId);

        HttpEntity<AllotmentPlanRoomUpdate> allotmentPlanRoomUpdateHttpEntity = new HttpEntity<>(
                allotmentPlanRoomUpdate,
                headers
        );

        restTemplate.exchange(
                serviceProperties.getUrl() + "/allotment-plans/" +
                        allotmentPlanId + "/rooms",
                HttpMethod.POST,
                allotmentPlanRoomUpdateHttpEntity,
                AllotmentPlanRoomUpdate.class
        );
    }
}
