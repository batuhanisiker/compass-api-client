package compass.client.service.compass;

import compass.client.service.HelperEntityService;
import compass.client.service.ServiceProperties;
import compass.model.ContractRatePlanList;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@EnableConfigurationProperties(ServiceProperties.class)
public class ContractRatePlanService {

    private final ServiceProperties serviceProperties;

    private HelperEntityService helperEntityService;

    private RestTemplate restTemplate;

    public ContractRatePlanService(ServiceProperties serviceProperties,
                                   HelperEntityService helperEntityService,
                                   RestTemplate restTemplate) {
        this.serviceProperties = serviceProperties;
        this.helperEntityService = helperEntityService;
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<ContractRatePlanList> getContractRatePlan(
            String consumerKey,
            String contractId) {

        HttpEntity<String> entity = helperEntityService.httpEntity(consumerKey, contractId);
        ResponseEntity<ContractRatePlanList> ratePlanListResponseEntity = restTemplate.exchange(
                serviceProperties.getUrl() + "/contracts/" + contractId + "/rate-plans",
                HttpMethod.GET, entity, new ParameterizedTypeReference<ContractRatePlanList>() {
                });

        return ratePlanListResponseEntity;
    }

    public ResponseEntity<ContractRatePlanList> getContractRatePlan(
            String consumerKey,
            String contractId,
            String embed) {

        HttpEntity<String> entity = helperEntityService.httpEntity(consumerKey, contractId);
        ResponseEntity<ContractRatePlanList> ratePlanListResponseEntity = restTemplate.exchange(
                serviceProperties.getUrl() + "/contracts/" + contractId + "/rate-plans?embed=" + embed,
                HttpMethod.GET, entity, new ParameterizedTypeReference<ContractRatePlanList>() {
                });

        return ratePlanListResponseEntity;
    }
}
