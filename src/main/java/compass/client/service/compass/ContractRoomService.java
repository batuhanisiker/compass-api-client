package compass.client.service.compass;


import compass.client.service.HelperEntityService;
import compass.client.service.ServiceProperties;
import compass.model.ContractRoom;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@EnableConfigurationProperties(ServiceProperties.class)
public class ContractRoomService {

    private final ServiceProperties serviceProperties;

    private HelperEntityService helperEntityService;

    private RestTemplate restTemplate;

    public ContractRoomService(ServiceProperties serviceProperties,
                               RestTemplate restTemplate,
                               HelperEntityService helperEntityService){

        this.serviceProperties = serviceProperties;
        this.restTemplate = restTemplate;
        this.helperEntityService = helperEntityService;
    }

    public ResponseEntity<List<ContractRoom>> getContractRoomList(String consumerKey, String contractId) {

        HttpEntity<String> entity = helperEntityService.httpEntity(consumerKey, contractId);
        ResponseEntity<List<ContractRoom>> roomResponseEntity = restTemplate.exchange(
                this.serviceProperties.getUrl() + "/contracts/" + contractId + "/rooms",
                HttpMethod.GET, entity, new ParameterizedTypeReference<List<ContractRoom>>() {
                });

        return roomResponseEntity;
    }
}
