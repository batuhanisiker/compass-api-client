package compass.client.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class HelperEntityService {

    private RestTemplate restTemplate;

    @Autowired
    public HelperEntityService(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }

    public HttpEntity<String> httpEntity(String password, String contractId) {

        HeaderService headerService = new HeaderService();

        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpHeaders headers = headerService.httpHeaderService(password, contractId);

        HttpEntity<String> entity = new HttpEntity<>(headers);

        return entity;
    }
}
