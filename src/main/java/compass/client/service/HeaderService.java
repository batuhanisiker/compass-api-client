package compass.client.service;

import org.springframework.http.HttpHeaders;

public class HeaderService {

    public HttpHeaders httpHeaderService(String password, String hotelCode) {
        HttpHeaders headers = new HttpHeaders();

        headers.set("x-consumer-key", password);
        headers.set("x-contract-id", hotelCode);
        headers.set("Content-Type", "application/json");
        return headers;
    }
}
