package compass.client.service;

import compass.model.ContractRoom;
import compass.model.RatePlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@EnableConfigurationProperties(ServiceProperties.class)
public class DataService {

    private final ServiceProperties serviceProperties;
    private RestTemplate restTemplate;
    private HelperEntityService helperEntityService;

    @Autowired
    public DataService(
                       ServiceProperties serviceProperties,
                       RestTemplate restTemplate,
                       HelperEntityService helperEntityService
    ) {
        this.serviceProperties = serviceProperties;
        this.restTemplate = restTemplate;
        this.helperEntityService = helperEntityService;
    }

    @Cacheable("ratePlan")
    public ResponseEntity<RatePlan>  getRatePlan(
            String contractId,
            Integer ratePlanCode,
            String embedString,
            String password
    ) {

        HttpEntity<String> entity = helperEntityService.httpEntity(password, contractId);

        ResponseEntity<RatePlan> ratePlanResponseEntity = restTemplate.exchange(
                serviceProperties.getUrl() + "/rate-plans/" + ratePlanCode + embedString,
                HttpMethod.GET, entity, RatePlan.class);

        return ratePlanResponseEntity;
    }
    @Cacheable("contractRoom")
    public ResponseEntity<ContractRoom> getContractRoom(
            String contractId,
            Integer roomCode,
            String password
    ) {

        HttpEntity<String> entity =helperEntityService.httpEntity(password, contractId);

        ResponseEntity<ContractRoom> contractRoomResponseEntity = restTemplate.exchange(
                serviceProperties.getUrl() +
                        "/contracts/" + contractId + "/rooms/" + roomCode,
                HttpMethod.GET, entity, ContractRoom.class);

        return contractRoomResponseEntity;
    }
}
